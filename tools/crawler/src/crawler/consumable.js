import axios from "axios";
import * as cheerio from 'cheerio';

const API = 'http://127.0.0.1:8080/graphql';
const BASE = 'https://www.dofus.com';
const LIST = '/fr/mmorpg/encyclopedie/consommables';

const START = 1;
const END = 1;

await main();

async function main() {
    for (let i = START; i <= END; i++) {
        if (i === 1) {
            await crawList(BASE + LIST);
        } else {
            await crawList(BASE + LIST + "?page=" + i);
        }
    }
}

async function crawList(url) {
    const response = await axios.get(url, {});

    const $ = cheerio.load(response.data);

    $('tbody tr td:nth-child(1) a').each(async (_, e) => {
        const url = $(e).attr('href');

        await crawlItem(BASE + url);
    });
}

async function crawlItem(url) {
    let stop = false;
    const response = await axios.get(url, {}).catch(e => {
        stop = true;
    });

    if (stop) {
        return;
    }

    const $ = cheerio.load(response.data);

    const name = $('h1').text().trim();
    const level = Number($('.ak-encyclo-detail-level').text().split(' ')[2]);
    const description = $('.ak-encyclo-detail-right .ak-panel-content:first').text().trim().replaceAll("\"", String.fromCharCode(92) + "\"").replaceAll("\n", " ");
    const type = $('.ak-encyclo-detail-type').text().trim().split(':')[1].trim();
    const imgUrl = $('.ak-encyclo-detail-illu img').attr('src');
    const gameId = Number(url.split("/")[7].split("-")[0]);
    const effects = crawlEffects($);
    const recipe = crawlRecipe($);
    const conditions = crawlConditions($).replaceAll("\n", " ");

    const query = `
mutation {
  createConsumable(consumable: {
    name: "${name}"
    gameId: ${gameId}
    level: ${level}
    url: "${url}"
    description: "${description}"
    imgUrl: "${imgUrl}"
    type: "${type}"
    effects: [
${effects}
    ]
    conditions: [
${conditions}
    ]
    recipe: [
${recipe}
    ]
  }) {
  id
  }
}`
    axios({
        url: `${API}`, method: 'post', data: {
            query: query
        }
    }).then((result) => {
        console.log(name);
        console.log(result.data);
    });
}

function crawlEffects($) {
    let effects = "";

    $('.ak-encyclo-detail-right .col-sm-6:nth-child(1) .ak-title').each((i, e) => {
        effects = effects + "\"" + ($(e).text().trim()) + "\"\n";
    });

    return effects;
}

function crawlConditions($) {
    let conditions = "";

    $('.ak-encyclo-detail-right .col-sm-6:nth-child(2) .ak-container:nth-child(1) .ak-container .ak-title').each((i, e) => {
        conditions = conditions + "\"" + ($(e).text().trim()) + "\"\n";
    });

    return conditions;
}

function crawlRecipe($) {
    let recipe = "";

    $('.ak-crafts .ak-list-element').each((i, e) => {
        const number = Number($(e).find(".ak-front").text().trim().split(" ")[0]);
        const name = $(e).find(".ak-linker").text().trim();
        recipe = recipe + "{ name: \"" + name + "\"\nnumber: " + number + "}";
    });
    return recipe;
}