import axios from "axios";
import * as cheerio from 'cheerio';

const API = 'http://127.0.0.1:8080/graphql';
const BASE = 'https://www.dofus.com';
const LIST = '/fr/mmorpg/encyclopedie/ressources';
const START = 1;
const END = 1;

await main();

async function main() {
    for (let i = START; i <= END; i++) {
        if (i === 1) {
            await crawlList(BASE + LIST);
        } else {
            await crawlList(BASE + LIST + "?page=" + i);
        }
    }
}

async function crawlList(url) {
    const response = await axios.get(url, {});

    const $ = cheerio.load(response.data);

    $('tbody tr td:nth-child(1) a').each(async (_, e) => {
        const url = $(e).attr('href');

        await crawlItem(BASE + url);
    });
}

async function crawlItem(url) {
    let stop = false;
    const response = await axios.get(url, {}).catch(e => {
        stop = true;
    });

    if (stop) {
        return;
    }

    const $ = cheerio.load(response.data);

    const name = $('h1').text().trim();
    const level = Number($('.ak-encyclo-detail-level').text().split(' ')[2]);
    const description = $('.ak-encyclo-detail-right .ak-panel-content:first').text().trim().replaceAll("\"", String.fromCharCode(92) + "\"").replaceAll("\n", " ");
    const type = $('.ak-encyclo-detail-type').text().trim().split(':')[1].trim();
    const imgUrl = $('.ak-encyclo-detail-illu img').attr('src');
    const gameId = Number(url.split("/")[7].split("-")[0]);

    axios({
        url: `${API}`, method: 'post', data: {
            query: `
                mutation {
                createResource(resource: {
                  name: "${name}"
                  gameId: ${gameId}
                  level: ${level}
                  url: "${url}"
                  description: "${description}"
                  imgUrl: "${imgUrl}"
                  type: "${type}"
                }) {
                  id
                  }
                }
              `
        }
    }).then((result) => {
        console.log(name);
        console.log(result.data)
    });
}