package domain.entity.weapon;

import domain.entity.recipe.RecipeItemEntityMutation;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.graphql.Input;
import org.eclipse.microprofile.graphql.NonNull;

@Input("CreateWeapon")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WeaponEntityMutation {
  @NonNull private Long gameId;
  @NonNull private String name;
  @NonNull private String description;
  @NonNull private Integer level;
  @NonNull private String type;
  @NonNull private String url;
  @NonNull private String imgUrl;
  @NonNull private Set<RecipeItemEntityMutation> recipe;
  @NonNull private Set<String> conditions;
  @NonNull private Set<String> effects;
  @NonNull private Set<String> characteristics;
}
