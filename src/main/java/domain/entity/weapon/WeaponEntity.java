package domain.entity.weapon;

import domain.entity.recipe.RecipeItemEntity;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.graphql.Type;

@Type("Weapon")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WeaponEntity {
  private Long id;
  private Long gameId;
  private String name;
  private String description;
  private Integer level;
  private String type;
  private String url;
  private String imgUrl;
  private Set<RecipeItemEntity> recipe;
  private Set<String> conditions;
  private Set<String> effects;
  private Set<String> characteristics;
}
