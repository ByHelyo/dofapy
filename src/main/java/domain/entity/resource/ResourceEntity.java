package domain.entity.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.graphql.Type;

@Type("Resource")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResourceEntity {
  private Long id;
  private Long gameId;
  private String name;
  private String description;
  private Integer level;
  private String type;
  private String url;
  private String imgUrl;
}
