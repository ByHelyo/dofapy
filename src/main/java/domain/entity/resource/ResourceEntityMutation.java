package domain.entity.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.graphql.Input;
import org.eclipse.microprofile.graphql.NonNull;

@Input("CreateResource")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResourceEntityMutation {
  @NonNull private Long gameId;
  @NonNull private String name;
  @NonNull private String description;
  @NonNull private Integer level;
  @NonNull private String type;
  @NonNull private String url;
  @NonNull private String imgUrl;
}
