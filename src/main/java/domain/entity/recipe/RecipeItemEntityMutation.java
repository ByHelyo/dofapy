package domain.entity.recipe;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.graphql.Input;

@Input("CreateRecipeItem")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecipeItemEntityMutation {
  private Integer number;
  private String name;
}
