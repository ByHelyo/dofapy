package domain.entity.recipe;

import domain.entity.resource.ResourceEntity;
import lombok.*;
import org.eclipse.microprofile.graphql.Type;

@Type("RecipeItem")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecipeItemEntity {
  private Integer number;
  private ResourceEntity resource;
}
