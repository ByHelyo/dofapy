package domain.entity.consumable;

import java.util.Set;

import domain.entity.recipe.RecipeItemEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.graphql.Type;

@Type("Consumable")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConsumableEntity {
  private Long id;
  private Long gameId;
  private String name;
  private String description;
  private Integer level;
  private String type;
  private String url;
  private String imgUrl;
  private Set<RecipeItemEntity> recipe;
  private Set<String> conditions;
  private Set<String> effects;
}