package domain.misc;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public interface Converter<Model, Entity> {

  Entity toEntity(Model model);

  Model toModel(Entity entity);

  default Set<Entity> toEntity(Collection<Model> models) {
    return models.stream().map(this::toEntity).collect(Collectors.toSet());
  }

  default Set<Model> toModel(Collection<Entity> entities) {
    return entities.stream().map(this::toModel).collect(Collectors.toSet());
  }
}
