package domain.service;

import data.model.WeaponModel;
import data.repository.WeaponRepository;
import domain.converter.WeaponConverter;
import domain.entity.weapon.WeaponEntity;
import domain.entity.weapon.WeaponEntityMutation;
import error.alreadyexists.WeaponAlreadyExistsException;
import error.notfound.WeaponNotFoundException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class WeaponService {
  @Inject WeaponRepository weaponRepository;

  @Inject WeaponConverter weaponConverter;

  public Set<WeaponEntity> getAllWeapons(String type) {
    Set<WeaponEntity> weaponEntities =
        weaponConverter.toEntity(weaponRepository.streamAll().collect(Collectors.toSet()));

    if (type != null) {
      return weaponEntities.stream()
          .filter(weaponEntity -> weaponEntity.getType().equals(type))
          .collect(Collectors.toSet());
    }

    return weaponEntities;
  }

  public WeaponEntity addWeapon(WeaponEntityMutation weaponEntityMutation) {
    if (weaponRepository.find("name", weaponEntityMutation.getName()).firstResult() != null) {
      throw new WeaponAlreadyExistsException(weaponEntityMutation.getName());
    }

    WeaponModel weaponModel = weaponConverter.fromEntityMutation(weaponEntityMutation);

    weaponRepository.persist(weaponModel);

    return weaponConverter.toEntity(weaponModel);
  }

  public WeaponEntity deleteWeapon(Long id) {
    WeaponModel weaponModel = weaponRepository.findById(id);

    if (weaponModel == null) {
      throw new WeaponNotFoundException(id);
    }

    weaponRepository.deleteById(id);
    return weaponConverter.toEntity(weaponModel);
  }
}
