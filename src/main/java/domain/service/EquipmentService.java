package domain.service;

import data.model.EquipmentModel;
import data.repository.EquipmentRepository;
import domain.converter.EquipmentConverter;
import domain.entity.equipment.EquipmentEntity;
import domain.entity.equipment.EquipmentEntityMutation;
import error.alreadyexists.EquipmentAlreadyExistsException;
import error.notfound.EquipmentNotFoundException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class EquipmentService {
  @Inject EquipmentRepository equipmentRepository;

  @Inject EquipmentConverter equipmentConverter;

  public Set<EquipmentEntity> getAllEquipments(String type) {
    Set<EquipmentEntity> equipmentEntities =
        equipmentConverter.toEntity(equipmentRepository.streamAll().collect(Collectors.toSet()));

    if (type != null) {
      return equipmentEntities.stream()
          .filter(equipmentEntity -> equipmentEntity.getType().equals(type))
          .collect(Collectors.toSet());
    }

    return equipmentEntities;
  }

  public EquipmentEntity addEquipment(EquipmentEntityMutation equipmentEntityMutation) {
    if (equipmentRepository.find("name", equipmentEntityMutation.getName()).firstResult() != null) {
      throw new EquipmentAlreadyExistsException(equipmentEntityMutation.getName());
    }

    EquipmentModel equipmentModel = equipmentConverter.fromEntityMutation(equipmentEntityMutation);

    equipmentRepository.persist(equipmentModel);

    return equipmentConverter.toEntity(equipmentModel);
  }

  public EquipmentEntity deleteEquipment(Long id) {
    EquipmentModel equipmentModel = equipmentRepository.findById(id);

    if (equipmentModel == null) {
      throw new EquipmentNotFoundException(id);
    }

    equipmentRepository.deleteById(id);
    return equipmentConverter.toEntity(equipmentModel);
  }
}
