package domain.service;

import data.model.ResourceModel;
import data.repository.ResourceRepository;
import domain.converter.ResourceConverter;
import domain.entity.resource.ResourceEntity;
import domain.entity.resource.ResourceEntityMutation;
import error.alreadyexists.ResourceAlreadyExistsException;
import error.notfound.ResourceNotFoundException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ResourceService {

  @Inject ResourceRepository resourceRepository;
  @Inject ResourceConverter resourceConverter;

  public Set<ResourceEntity> getAllResources(String type) {
    Set<ResourceEntity> resourceEntities =
        resourceConverter.toEntity(resourceRepository.streamAll().collect(Collectors.toSet()));

    if (type != null) {
      return resourceEntities.stream()
          .filter(resourceEntity -> resourceEntity.getType().equals(type))
          .collect(Collectors.toSet());
    }

    return resourceEntities;
  }

  public ResourceEntity getResource(Long id) {
    ResourceModel resourceModel = resourceRepository.findById(id);
    return resourceConverter.toEntity(resourceModel);
  }

  public ResourceEntity addResource(ResourceEntityMutation resourceEntityMutation) {
    if (resourceRepository.find("name", resourceEntityMutation.getName()).firstResult() != null) {
      throw new ResourceAlreadyExistsException(resourceEntityMutation.getName());
    }

    ResourceModel resourceModel = resourceConverter.fromEntityMutation(resourceEntityMutation);

    resourceRepository.persist(resourceModel);
    return resourceConverter.toEntity(resourceModel);
  }

  public ResourceEntity deleteResource(Long id) {
    ResourceModel resourceModel = resourceRepository.findById(id);

    if (resourceModel == null) {
      throw new ResourceNotFoundException(id);
    }

    resourceRepository.deleteById(id);
    return resourceConverter.toEntity(resourceModel);
  }
}
