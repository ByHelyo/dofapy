package domain.service;

import data.model.RecipeItemModel;
import data.model.ResourceModel;
import data.repository.ResourceRepository;
import domain.entity.recipe.RecipeItemEntityMutation;
import error.notfound.ResourceNotFoundException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class RecipeService {

  @Inject ResourceRepository resourceRepository;

  public RecipeItemModel findRecipeOrCreate(String name, Integer number) {

    ResourceModel resourceModel = resourceRepository.find("name", name).firstResult();

    if (resourceModel == null) {
      throw new ResourceNotFoundException(name);
    }

    return new RecipeItemModel(null, number, resourceModel);
  }

  public Set<RecipeItemModel> findRecipeOrCreate(
      Set<RecipeItemEntityMutation> recipeItemEntityMutationSet) {
    return recipeItemEntityMutationSet.stream()
        .map(recipe -> findRecipeOrCreate(recipe.getName(), recipe.getNumber()))
        .collect(Collectors.toSet());
  }
}
