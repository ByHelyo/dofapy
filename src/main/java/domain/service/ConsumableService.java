package domain.service;

import data.model.ConsumableModel;
import data.repository.ConsumableRepository;
import domain.converter.ConsumableConverter;
import domain.entity.consumable.ConsumableEntity;
import domain.entity.consumable.ConsumableEntityMutation;
import error.alreadyexists.ConsumableAlreadyExistsException;
import error.notfound.ConsumableNotFoundException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ConsumableService {
  @Inject ConsumableRepository consumableRepository;
  @Inject ConsumableConverter consumableConverter;

  public Set<ConsumableEntity> getAllConsumables(String type) {

    Set<ConsumableEntity> consumableEntities =
        consumableConverter.toEntity(consumableRepository.streamAll().collect(Collectors.toSet()));

    if (type != null) {
      return consumableEntities.stream()
          .filter(consumableEntity -> consumableEntity.getType().equals(type))
          .collect(Collectors.toSet());
    }

    return consumableEntities;
  }

  public ConsumableEntity addConsumable(ConsumableEntityMutation consumableEntityMutation) {
    if (consumableRepository.find("name", consumableEntityMutation.getName()).firstResult()
        != null) {
      throw new ConsumableAlreadyExistsException(consumableEntityMutation.getName());
    }

    ConsumableModel consumableModel =
        consumableConverter.fromEntityMutation(consumableEntityMutation);

    consumableRepository.persist(consumableModel);

    return consumableConverter.toEntity(consumableModel);
  }

  public ConsumableEntity deleteConsumable(Long id) {
    ConsumableModel consumableModel = consumableRepository.findById(id);

    if (consumableModel == null) {
      throw new ConsumableNotFoundException(id);
    }

    consumableRepository.deleteById(id);
    return consumableConverter.toEntity(consumableModel);
  }
}
