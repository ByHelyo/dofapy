package domain.converter;

import data.model.RecipeItemModel;
import domain.entity.recipe.RecipeItemEntity;
import domain.misc.Converter;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class RecipeItemConverter implements Converter<RecipeItemModel, RecipeItemEntity> {

  @Inject ResourceConverter resourceConverter;

  public RecipeItemEntity toEntity(RecipeItemModel recipeItemModel) {
    return new RecipeItemEntity(
        recipeItemModel.getNumber(), resourceConverter.toEntity(recipeItemModel.getResource()));
  }

  public RecipeItemModel toModel(RecipeItemEntity recipeItemEntity) {
    return new RecipeItemModel(
        null,
        recipeItemEntity.getNumber(),
        resourceConverter.toModel(recipeItemEntity.getResource()));
  }
}
