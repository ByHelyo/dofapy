package domain.converter;

import data.model.EquipmentModel;
import data.model.RecipeItemModel;
import domain.entity.equipment.EquipmentEntity;
import domain.entity.equipment.EquipmentEntityMutation;
import domain.misc.Converter;
import domain.service.RecipeService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;

@ApplicationScoped
public class EquipmentConverter implements Converter<EquipmentModel, EquipmentEntity> {

  @Inject RecipeService recipeService;

  @Inject RecipeItemConverter recipeItemConverter;

  public EquipmentEntity toEntity(EquipmentModel equipmentModel) {
    return new EquipmentEntity(
        equipmentModel.getId(),
        equipmentModel.getGameId(),
        equipmentModel.getName(),
        equipmentModel.getDescription(),
        equipmentModel.getLevel(),
        equipmentModel.getType(),
        equipmentModel.getUrl(),
        equipmentModel.getImgUrl(),
        recipeItemConverter.toEntity(equipmentModel.getRecipe()),
        equipmentModel.getConditions(),
        equipmentModel.getEffects());
  }

  public EquipmentModel toModel(EquipmentEntity equipmentEntity) {
    return new EquipmentModel(
        equipmentEntity.getId(),
        equipmentEntity.getGameId(),
        equipmentEntity.getName(),
        equipmentEntity.getDescription(),
        equipmentEntity.getLevel(),
        equipmentEntity.getType(),
        equipmentEntity.getUrl(),
        equipmentEntity.getImgUrl(),
        recipeItemConverter.toModel(equipmentEntity.getRecipe()),
        equipmentEntity.getConditions(),
        equipmentEntity.getEffects());
  }

  public EquipmentModel fromEntityMutation(EquipmentEntityMutation equipmentEntityMutation) {
    Set<RecipeItemModel> recipeItemModels =
        recipeService.findRecipeOrCreate(equipmentEntityMutation.getRecipe());

    return new EquipmentModel(
        null,
        equipmentEntityMutation.getGameId(),
        equipmentEntityMutation.getName(),
        equipmentEntityMutation.getDescription(),
        equipmentEntityMutation.getLevel(),
        equipmentEntityMutation.getType(),
        equipmentEntityMutation.getUrl(),
        equipmentEntityMutation.getImgUrl(),
        recipeItemModels,
        equipmentEntityMutation.getConditions(),
        equipmentEntityMutation.getEffects());
  }
}
