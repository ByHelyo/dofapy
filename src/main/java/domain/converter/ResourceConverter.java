package domain.converter;

import data.model.ResourceModel;
import domain.entity.resource.ResourceEntity;
import domain.entity.resource.ResourceEntityMutation;
import domain.misc.Converter;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ResourceConverter implements Converter<ResourceModel, ResourceEntity> {
  public ResourceEntity toEntity(ResourceModel resourceModel) {
    return new ResourceEntity(
        resourceModel.getId(),
        resourceModel.getGameId(),
        resourceModel.getName(),
        resourceModel.getDescription(),
        resourceModel.getLevel(),
        resourceModel.getType(),
        resourceModel.getUrl(),
        resourceModel.getImgUrl());
  }

  public ResourceModel toModel(ResourceEntity resourceEntity) {
    return new ResourceModel(
        resourceEntity.getId(),
        resourceEntity.getGameId(),
        resourceEntity.getName(),
        resourceEntity.getDescription(),
        resourceEntity.getLevel(),
        resourceEntity.getType(),
        resourceEntity.getUrl(),
        resourceEntity.getImgUrl());
  }

  public ResourceModel fromEntityMutation(ResourceEntityMutation resourceEntityMutation) {
    return new ResourceModel(
        null,
        resourceEntityMutation.getGameId(),
        resourceEntityMutation.getName(),
        resourceEntityMutation.getDescription(),
        resourceEntityMutation.getLevel(),
        resourceEntityMutation.getType(),
        resourceEntityMutation.getUrl(),
        resourceEntityMutation.getImgUrl());
  }
}
