package domain.converter;

import data.model.ConsumableModel;
import data.model.RecipeItemModel;
import domain.entity.consumable.ConsumableEntity;
import domain.entity.consumable.ConsumableEntityMutation;
import domain.misc.Converter;
import domain.service.RecipeService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;

@ApplicationScoped
public class ConsumableConverter implements Converter<ConsumableModel, ConsumableEntity> {

  @Inject RecipeService recipeService;

  @Inject RecipeItemConverter recipeItemConverter;

  public ConsumableEntity toEntity(ConsumableModel consumableModel) {
    return new ConsumableEntity(
        consumableModel.getId(),
        consumableModel.getGameId(),
        consumableModel.getName(),
        consumableModel.getDescription(),
        consumableModel.getLevel(),
        consumableModel.getType(),
        consumableModel.getUrl(),
        consumableModel.getImgUrl(),
        recipeItemConverter.toEntity(consumableModel.getRecipe()),
        consumableModel.getConditions(),
        consumableModel.getEffects());
  }

  public ConsumableModel toModel(ConsumableEntity consumableEntity) {
    return new ConsumableModel(
        consumableEntity.getId(),
        consumableEntity.getGameId(),
        consumableEntity.getName(),
        consumableEntity.getDescription(),
        consumableEntity.getLevel(),
        consumableEntity.getType(),
        consumableEntity.getUrl(),
        consumableEntity.getImgUrl(),
        recipeItemConverter.toModel(consumableEntity.getRecipe()),
        consumableEntity.getConditions(),
        consumableEntity.getEffects());
  }

  public ConsumableModel fromEntityMutation(ConsumableEntityMutation consumableEntityMutation) {
    Set<RecipeItemModel> recipeItemModels =
        recipeService.findRecipeOrCreate(consumableEntityMutation.getRecipe());

    return new ConsumableModel(
        null,
        consumableEntityMutation.getGameId(),
        consumableEntityMutation.getName(),
        consumableEntityMutation.getDescription(),
        consumableEntityMutation.getLevel(),
        consumableEntityMutation.getType(),
        consumableEntityMutation.getUrl(),
        consumableEntityMutation.getImgUrl(),
        recipeItemModels,
        consumableEntityMutation.getConditions(),
        consumableEntityMutation.getEffects());
  }
}
