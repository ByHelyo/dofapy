package domain.converter;

import data.model.RecipeItemModel;
import data.model.WeaponModel;
import domain.entity.weapon.WeaponEntity;
import domain.entity.weapon.WeaponEntityMutation;
import domain.misc.Converter;
import domain.service.RecipeService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;

@ApplicationScoped
public class WeaponConverter implements Converter<WeaponModel, WeaponEntity> {

  @Inject RecipeService recipeService;
  @Inject RecipeItemConverter recipeItemConverter;

  public WeaponEntity toEntity(WeaponModel weaponModel) {
    return new WeaponEntity(
        weaponModel.getId(),
        weaponModel.getGameId(),
        weaponModel.getName(),
        weaponModel.getDescription(),
        weaponModel.getLevel(),
        weaponModel.getType(),
        weaponModel.getUrl(),
        weaponModel.getImgUrl(),
        recipeItemConverter.toEntity(weaponModel.getRecipe()),
        weaponModel.getConditions(),
        weaponModel.getEffects(),
        weaponModel.getCharacteristics());
  }

  public WeaponModel toModel(WeaponEntity weaponEntity) {
    return new WeaponModel(
        weaponEntity.getId(),
        weaponEntity.getGameId(),
        weaponEntity.getName(),
        weaponEntity.getDescription(),
        weaponEntity.getLevel(),
        weaponEntity.getType(),
        weaponEntity.getUrl(),
        weaponEntity.getImgUrl(),
        recipeItemConverter.toModel(weaponEntity.getRecipe()),
        weaponEntity.getConditions(),
        weaponEntity.getEffects(),
        weaponEntity.getCharacteristics());
  }

  public WeaponModel fromEntityMutation(WeaponEntityMutation weaponEntityMutation) {
    Set<RecipeItemModel> recipeItemModels =
        recipeService.findRecipeOrCreate(weaponEntityMutation.getRecipe());

    return new WeaponModel(
        null,
        weaponEntityMutation.getGameId(),
        weaponEntityMutation.getName(),
        weaponEntityMutation.getDescription(),
        weaponEntityMutation.getLevel(),
        weaponEntityMutation.getType(),
        weaponEntityMutation.getUrl(),
        weaponEntityMutation.getImgUrl(),
        recipeItemModels,
        weaponEntityMutation.getConditions(),
        weaponEntityMutation.getEffects(),
        weaponEntityMutation.getCharacteristics());
  }
}
