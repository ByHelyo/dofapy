package presentation;

import domain.entity.weapon.WeaponEntity;
import domain.entity.weapon.WeaponEntityMutation;
import domain.service.WeaponService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import java.util.Set;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

@GraphQLApi
public class WeaponResource {
  @Inject WeaponService weaponService;

  @Query("allWeapons")
  public Set<WeaponEntity> getAllWeapons(String type) {
    return weaponService.getAllWeapons(type);
  }

  @Mutation
  @Transactional
  public WeaponEntity createWeapon(@Name("weapon") WeaponEntityMutation weaponEntityMutation) {
    WeaponEntity weaponEntity = weaponService.addWeapon(weaponEntityMutation);

    return weaponEntity;
  }

  @Mutation
  @Transactional
  public WeaponEntity deleteWeapon(Long id) {
    return weaponService.deleteWeapon(id);
  }
}
