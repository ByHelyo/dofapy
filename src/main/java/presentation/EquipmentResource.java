package presentation;

import domain.entity.equipment.EquipmentEntity;
import domain.entity.equipment.EquipmentEntityMutation;
import domain.service.EquipmentService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import java.util.Set;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

@GraphQLApi
public class EquipmentResource {
  @Inject EquipmentService equipmentService;

  @Query("allEquipments")
  public Set<EquipmentEntity> getAllEquipments(String type) {
    return equipmentService.getAllEquipments(type);
  }

  @Mutation
  @Transactional
  public EquipmentEntity createEquipment(
      @Name("equipment") EquipmentEntityMutation equipmentEntityMutation) {
    EquipmentEntity equipmentEntity = equipmentService.addEquipment(equipmentEntityMutation);

    return equipmentEntity;
  }

  @Mutation
  @Transactional
  public EquipmentEntity deleteEquipment(Long id) {
    return equipmentService.deleteEquipment(id);
  }
}
