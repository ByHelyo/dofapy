package presentation;

import domain.entity.resource.ResourceEntity;
import domain.entity.resource.ResourceEntityMutation;
import domain.service.ResourceService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import java.util.Set;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

@GraphQLApi
public class ResourceResource {

  @Inject ResourceService resourceService;

  @Query("allResources")
  public Set<ResourceEntity> getAllResources(String type) {

    return resourceService.getAllResources(type);
  }

  @Query
  public ResourceEntity getResource(Long id) {
    return resourceService.getResource(id);
  }

  @Transactional
  @Mutation
  public ResourceEntity createResource(
      @Name("resource") ResourceEntityMutation resourceEntityMutation) {
    return resourceService.addResource(resourceEntityMutation);
  }

  @Transactional
  @Mutation
  public ResourceEntity deleteResource(Long id) {
    return resourceService.deleteResource(id);
  }
}
