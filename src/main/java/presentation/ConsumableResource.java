package presentation;

import domain.entity.consumable.ConsumableEntity;
import domain.entity.consumable.ConsumableEntityMutation;
import domain.service.ConsumableService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import java.util.Set;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

@GraphQLApi
public class ConsumableResource {

  @Inject ConsumableService consumableService;

  @Query("allConsumables")
  public Set<ConsumableEntity> getAllConsumables(String type) {
    return consumableService.getAllConsumables(type);
  }

  @Mutation
  @Transactional
  public ConsumableEntity createConsumable(
      @Name("consumable") ConsumableEntityMutation consumableEntityMutation) {
    ConsumableEntity consumableEntity = consumableService.addConsumable(consumableEntityMutation);

    return consumableEntity;
  }

  @Mutation
  @Transactional
  public ConsumableEntity deleteConsumable(Long id) {
    return consumableService.deleteConsumable(id);
  }
}
