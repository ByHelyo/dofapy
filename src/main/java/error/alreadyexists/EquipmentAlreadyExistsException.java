package error.alreadyexists;

import io.smallrye.graphql.api.ErrorCode;

@ErrorCode("EQUIPMENT_ALREADY_EXISTS")
public class EquipmentAlreadyExistsException extends RuntimeException {
  public EquipmentAlreadyExistsException(String name) {
    super("Equipment: \"" + name + "\" already exists");
  }
}
