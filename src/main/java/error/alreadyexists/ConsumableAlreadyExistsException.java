package error.alreadyexists;

import io.smallrye.graphql.api.ErrorCode;

@ErrorCode("CONSUMABLE_ALREADY_EXISTS")
public class ConsumableAlreadyExistsException extends RuntimeException {
    public ConsumableAlreadyExistsException(String name) {
        super("Consumable: \"" + name + "\" already exists");
    }
}
