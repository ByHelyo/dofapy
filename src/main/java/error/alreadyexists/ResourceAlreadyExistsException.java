package error.alreadyexists;

import io.smallrye.graphql.api.ErrorCode;

@ErrorCode("RESOURCE_ALREADY_EXISTS")
public class ResourceAlreadyExistsException extends RuntimeException {
  public ResourceAlreadyExistsException(String name) {
    super("Resource: \"" + name + "\" already exists");
  }
}
