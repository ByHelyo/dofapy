package error.alreadyexists;

import io.smallrye.graphql.api.ErrorCode;

@ErrorCode("WEAPON_ALREADY_EXIST")
public class WeaponAlreadyExistsException extends RuntimeException {
  public WeaponAlreadyExistsException(String name) {
    super("Weapon: \"" + name + "\" already exists");
  }
}
