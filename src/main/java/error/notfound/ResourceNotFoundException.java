package error.notfound;

import io.smallrye.graphql.api.ErrorCode;

@ErrorCode("RESOURCE_NOT_FOUND")
public class ResourceNotFoundException extends RuntimeException {
  public ResourceNotFoundException(Long id) {
    super("Resource: id" + id + " not found");
  }

  public ResourceNotFoundException(String name) {
    super("Resource: \"" + name + "\" not found");
  }
}
