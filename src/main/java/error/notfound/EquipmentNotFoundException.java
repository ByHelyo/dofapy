package error.notfound;

import io.smallrye.graphql.api.ErrorCode;

@ErrorCode("EQUIPMENT_NOT_FOUND")
public class EquipmentNotFoundException extends RuntimeException {
  public EquipmentNotFoundException(Long id) {
    super("Equipment: id" + id + " not found");
  }
}
