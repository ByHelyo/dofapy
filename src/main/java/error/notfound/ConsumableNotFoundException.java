package error.notfound;

import io.smallrye.graphql.api.ErrorCode;

@ErrorCode("CONSUMABLE_NOT_FOUND")
public class ConsumableNotFoundException extends RuntimeException {
  public ConsumableNotFoundException(Long id) {
    super("Consumable: id" + id + " not found");
  }
}
