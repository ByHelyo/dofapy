package data.model;

import jakarta.persistence.*;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "equipment")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EquipmentModel {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false)
  private Long gameId;

  @Column(nullable = false, unique = true)
  private String name;

  @Column(nullable = false, columnDefinition = "TEXT")
  private String description;

  @Column(nullable = false)
  private Integer level;

  @Column(nullable = false)
  private String type;

  @Column(nullable = false)
  private String url;

  @Column(nullable = false, name = "img_url")
  private String imgUrl;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "equipment_id")
  private Set<RecipeItemModel> recipe;

  @ElementCollection(targetClass = String.class)
  @CollectionTable(name = "condition_equipment", joinColumns = @JoinColumn(name = "equipment_id"))
  @Column(nullable = false, name = "condition")
  private Set<String> conditions;

  @ElementCollection(targetClass = String.class)
  @CollectionTable(name = "effect_equipment", joinColumns = @JoinColumn(name = "equipment_id"))
  @Column(nullable = false, name = "effect")
  private Set<String> effects;
}
