package data.repository;

import data.model.EquipmentModel;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EquipmentRepository implements PanacheRepository<EquipmentModel> {}
